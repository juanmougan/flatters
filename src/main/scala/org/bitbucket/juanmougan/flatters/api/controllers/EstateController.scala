package org.bitbucket.juanmougan.flatters.api

import org.scalatra._
import org.bitbucket.juanmougan.flatters._
// JSON-related libraries
import org.json4s.{DefaultFormats, Formats}
// JSON handling support from Scalatra
import org.scalatra.json._

class EstateController extends ScalatraServlet with JacksonJsonSupport {

  // Sets up automatic case class to JSON output serialization, required by
  // the JValueResult trait.
  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  // Before every action runs, set the content type to be in JSON format.
  before() {
    contentType = formats("json")
  }

  get("/estate") {
    EstateData.all
  }

  post("/estate") {
    val estate = parsedBody.extract[Estate]
    EstateData.all :+ estate
  }

}

// Fake model
case class Estate(id: Long, name: String, lat: Double, long: Double)

object EstateData {

  /**
   * Some fake flats data so we can simulate retrievals.
   */
  var all = List(
      Estate(1, "Departamento en Palermo", -34.594142, -58.422036),
      Estate(2, "Casa en San Isidro", -34.46761, -58.510191),
      Estate(3, "Galpón en Isidro Casanova", -34.7108671, -58.5958651)
    )
}
