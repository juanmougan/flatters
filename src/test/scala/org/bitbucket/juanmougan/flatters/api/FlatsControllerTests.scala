package org.bitbucket.juanmougan.flatters.api

import org.scalatra.test.scalatest._

class FlatsControllerTests extends ScalatraFunSuite {

  addServlet(classOf[FlatsController], "/*")

  test("GET / on FlatsController should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

}
